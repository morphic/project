from app.users.models import User
from rest_framework import serializers
from app.users.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["name", "last_name", "email", "is_active", "is_staff", "is_superuser", "age"]
